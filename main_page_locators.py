from selenium.webdriver.common.by import By


class MainPageLocators:
    ELEM_LINK = (By.XPATH, '//h1[text()="Automation Practice Website"]')
    LOGIN_LINK = (By.CLASS_NAME, 'login')
    Elem_on_login_page = (By.CLASS_NAME, 'page-heading')
    BASKET_LINK = (By.XPATH, '//b[text()="Cart"]')
    Empty_Basket = (By.XPATH, '//p[text()="Your shopping cart is empty."]')
