from selenium import webdriver
from pytest import fixture


@fixture()
def browser():
    print("Start browser")
    driver = webdriver.Chrome()
    yield driver
    print('Quit browser')
    driver.quit()
