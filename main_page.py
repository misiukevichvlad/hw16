from pages.base_page import BasePage
from locators.main_page_locators import MainPageLocators


class MainPage(BasePage):
    def should_be_elem_on_main_page(self):
        assert self.is_elem_present(*MainPageLocators.ELEM_LINK), 'No such element'

    def open_main_page(self):
        self.driver.get(self.url)
        return MainPage(driver=self.driver, url=self.driver.current_url)

    def should_be_main_page(self):
        link1 = 'http://automationpractice.com/index.php'
        assert self.driver.current_url == link1
