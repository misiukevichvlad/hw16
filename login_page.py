import time
from pages.base_page import BasePage
from locators.main_page_locators import MainPageLocators


class LoginPage(BasePage):
    def open_login_page(self):
        login_link = self.driver.find_element(*MainPageLocators.LOGIN_LINK)
        login_link.click()
        return LoginPage(driver=self.driver, url=self.driver.current_url)

    def should_be_login_page(self):
        link = 'http://automationpractice.com/index.php?controller=authentication&back=my-account'
        assert self.driver.current_url == link

    def should_be_elem_on_login_page(self):
        time.sleep(1)
        assert self.is_elem_present(*MainPageLocators.Elem_on_login_page), 'No such element'
