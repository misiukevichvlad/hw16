class BasePage:
    def __init__(self, driver, url):
        self.driver = driver
        self.url = url

    def open(self):
        self.driver.get(self.url)

    def is_elem_present(self, locate_by, selector):
        try:
            self.driver.find_element(locate_by, selector)
        except:
            return False
        return True
