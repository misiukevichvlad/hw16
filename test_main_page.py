from pages.basket_page import BasketPage
from pages.login_page import LoginPage
from pages.main_page import MainPage


def test_is_it_main_page(browser):
    url = 'http://automationpractice.com/index.php'
    page = MainPage(browser, url)
    main_page = page.open_main_page()
    main_page.should_be_elem_on_main_page()
    main_page.should_be_main_page()


def test_is_it_login_page(browser):
    url = 'http://automationpractice.com/index.php'
    page = LoginPage(browser, url)
    page.open()
    login_page = page.open_login_page()
    login_page.should_be_login_page()


def test_is_it_basket_page(browser):
    url = 'http://automationpractice.com/index.php'
    page = BasketPage(browser, url)
    page.open()
    basket_page = page.open_basket_page()
    basket_page.should_be_basket_page()


def test_user_can_open_login_page(browser):
    url = 'http://automationpractice.com/index.php'
    page = LoginPage(browser, url)
    page.open()
    page.open_login_page()
    page.should_be_elem_on_login_page()


def test_is_basket_empty(browser):
    url = 'http://automationpractice.com/index.php'
    page = BasketPage(browser, url)
    page.open()
    page.open_basket_page()
    page.basket_should_be_empty()
